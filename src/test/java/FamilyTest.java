import HomeWork6.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class FamilyTest {

    @Test
    void ToString() {
        Pet rabbit = new Pet(Species.RABBIT, "jackie", 1, 75, new String[]{"eat", "drink", "sleep"});
        Human mother = new Human("jane ", "Karleon", 57);
        Human father = new Human("john", "Karleon", 37);
        Human child = new Human("Jack", "Karleon", 10, 98, mother, father, rabbit, new String[][]{new String[]{DayOfWeek.Sunday.name(), "Go for a walk"}});
        Family family = new Family(mother, father);
        family.addChild(child);
        String expected = "Human{name='Jack', surname='Karleon', year=10, iq=98, RABBIT{fly=false, howLegsHaveGot=4, canSwim=true} {nickname= 'jackie', age=1, trickLevel=75, habits=[eat, drink, sleep]schedule={[Sunday,Go for a walk]}}}";
        assertEquals(expected, family.toString());
    }

    @Test
    void countFamily() {
        Human father = new Human("Peter", "Karleon", 27);
        Human mother = new Human("Elisa", "Karleon", 30);
        Human child = new Human("Bill", "Karleon", 17);
        Human child1 = new Human("Bob", "Karleon", 14);
        Human child2 = new Human("Lisa", "Karleon", 13);
        // Human child3 = new Human("Tom", "Karleon", 12);
        Family family = new Family(mother, father);
        family.addChild(child);
        family.addChild(child1);
        family.addChild(child2);
        int result = family.countFamily();
        assertEquals(5, result);
    }

    @Test
    void addChild() {
        Human father = new Human("Jane", "Karleon", 15);
        Human mother = new Human("John", "Karleon", 14);
        Human child1 = new Human("Bill", "Karleon", 13);
        Family family = new Family(mother, father);
        family.addChild(child1);
        int result = family.getChildren().length;
        Human child = family.getChildren()[family.getChildren().length - 1];
        assertEquals(1, result);
        assertEquals(child1, child);

    }

    @Test
    void deleteChild() {
        Human father = new Human("Peter", "Karleon", 26);
        Human mother = new Human("Elisa", "Karleon", 24);
        Human child = new Human("Bill", "Karleon", 3);
        Human child1 = new Human("Bob", "Karleon", 5);
        Human child2 = new Human("Lisa", "Karleon", 7);
        Human child3 = new Human("Tom", "Karleon", 4);
        Family family = new Family(mother, father);
        family.addChild(child);
        family.addChild(child1);
        family.addChild(child2);
        family.deleteChild(child2);


        int result = family.getChildren().length;
        assertEquals(2, result);

        family.deleteChild(child3);
        int res = family.getChildren().length;
        assertEquals(2, res);

    }

    @Test
    void deleteChildWithIndex() {
        Human father = new Human("Peter", "Karleon", 26);
        Human mother = new Human("Elisa", "Karleon", 24);
        Human child = new Human("Bill", "Karleon", 3);
        Human child1 = new Human("Bob", "Karleon", 5);
        Human child2 = new Human("Lisa", "Karleon", 7);
        // Human child3 = new Human("Tom", "Karleon", 4);
        Family family = new Family(mother, father);
        family.addChild(child);
        family.addChild(child1);
        family.addChild(child2);
        family.deleteChildWithIndex(3);
        int result = family.getChildren().length;
        assertEquals(2, result);
        family.deleteChildWithIndex(2);
        int result1 = family.getChildren().length + 1;
        assertEquals(2, result1);
    }
}